package com.fortech.sensordata.controller;

import java.util.Optional;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("data-sets")
public class DataSetController {

  @GetMapping(value = "{id}", produces = "application/json")
  public ResponseEntity<InputStreamResource> readDataSet(@PathVariable("id") Integer id) {
    return Optional.ofNullable(DataSetController.class.getResourceAsStream("/" + id.toString()))
        .map(InputStreamResource::new)
        .map(ResponseEntity::ok)
        .orElse(ResponseEntity.notFound().build());
  }
}
